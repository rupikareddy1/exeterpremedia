const studentapp = require('fastify')({
    logger: true
})

const processRoutes = require('./route/process')
processRoutes.forEach((route, index) => {
    studentapp.route(route)
})

// Run the server!
studentapp.listen(3000, (err, address) => {
    if (err) {
        studentapp.log.error(err)
        process.exit(1)
    }
    studentapp.log.info(`server listening on ${address}`)
})