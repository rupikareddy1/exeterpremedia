var students = []

const report = async (req, reply) => {
    return students
}

const add = async (req, reply) => {
    const newRecord = {
        studentname:req.body.studentname,
        studentid:req.body.studentid,
        subject1: req.body.subject1,
        subject2: req.body.subject2,
        subject3: req.body.subject3,
        subject4: req.body.subject4,
        subject5: req.body.subject5
        
    }
   
   for(var i=0 ; i<students.length ;i++)
   { //uniqueness
    var student= students[i];
   if(newRecord.studentid == student.studentid)
   {
     return { msg: ` the entered id  is duplicate ` }
    var flag=0;
   }
}
   if(flag!=0)
   {
    students.push(newRecord)
    return newRecord
   }

}    

const deletefun = async (req, reply) => {
    const studentid = Number(req.params.studentid)

    students = students.filter(s => s.studentid !== studentid)
    return { msg: `Record with ID ${studentid} is deleted` }
}

const update = async (req, reply) => {
    const studentid = Number(req.params.studentid)
    var studentup = students.map(s => {
        if (s.studentid === studentid) {
            return {
                studentid,
                subject1:req.body.subject1,
                subject2:req.body.subject2,
                subject3:req.body.subject3,
                subject4:req.body.subject4,
                subject5:req.body.subject5
                
            }
        }
    })
      
    students.push(studentup)


    return {
                studentid,
                subject1:req.body.subject1,
                subject2:req.body.subject2,
                subject3:req.body.subject3,
                subject4:req.body.subject4,
                subject5:req.body.subject5
       
    }
   

}


module.exports = {
    report, //to generate the list of values in the object
    add, //to add new record
    deletefun, //to delete the given record
    update //to update record according to student id
}