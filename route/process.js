const processController = require('../operation/process');

const routes = [{
        method: 'GET',
        url: '/api/students',
        handler: processController.report
    },
    {
        method: 'POST',
        url: '/api/students',
        handler: processController.add
    },
    {
        method: 'DELETE',
        url: '/api/students/:studentid',
        handler: processController.deletefun
    },
    {
        method: 'POST',
        url: '/api/students/:studentid',
        handler: processController.update
    }
]
module.exports = routes